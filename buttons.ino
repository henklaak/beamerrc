#include "pindefs.h"
#include "buttons.h"

#define BTN_PRESSED (HIGH)
#define LONG_PRESS_TIMEOUT (1000) // ms

bool btnOffPressed = false;
bool btnOnPressed = false;

unsigned long btn_off_timeout = 0;
unsigned long btn_on_timeout = 0;

extern void (*resetFunc) ();

/**************************************************************************************************/
void buttons_init()
{
  btnOffPressed = false;
  btnOnPressed = false;
  btn_off_timeout = 0;
  btn_on_timeout = 0;
  
  pinMode(IN_OFF, INPUT);
  pinMode(IN_ON, INPUT);
}

/**************************************************************************************************/
void buttons(unsigned long timestamp)
{
  // Pressing both buttons causes a reset
  if (btnOffPressed && btnOnPressed)
  {
    resetFunc();
  }

  if (digitalRead(IN_OFF) == BTN_PRESSED)
  {
    if (!btn_off_timeout)
    {
      btn_off_timeout = timestamp + LONG_PRESS_TIMEOUT;
    }
    btnOffPressed = (timestamp > btn_off_timeout);
  }
  else
  {
    btn_off_timeout  = 0;
    btnOffPressed = false;
  }

  if (digitalRead(IN_ON) == BTN_PRESSED)
  {
    if (!btn_on_timeout)
    {
      btn_on_timeout = timestamp + LONG_PRESS_TIMEOUT;
    }
    btnOnPressed = (timestamp > btn_on_timeout);
  }
  else
  {
    btn_on_timeout  = 0;
    btnOnPressed = false;
  }
}
