#include "beamer.h"

#define REPLY_TIMEOUT  (15000) // ms

BeamerState beamer_state = BEAMERSTATE_UNKNOWN;
unsigned long reply_timeout = 0;
String replyString = "";


extern void (*resetFunc) ();

/**************************************************************************************************/
void beamer_init()
{
  beamer_state = BEAMERSTATE_UNKNOWN;
  reply_timeout = 0;
  replyString.reserve(64);
  replyString = "";

  Serial1.begin(19200, SERIAL_8N1);
  while (!Serial1) {
    delay(10);
  }
}

/**************************************************************************************************/
bool beamer_is_busy(unsigned long timestamp)
{
  // Comm timeout causes a reset
  if (reply_timeout and timestamp >= reply_timeout)
  {
    resetFunc();
  }
  return reply_timeout;
}

/**************************************************************************************************/
void beamer(unsigned long timestamp, BeamerCommand cmd)
{
  if (reply_timeout) // previous command is still pending, state machine error
  {
    resetFunc();
  }

  switch (cmd)
  {
    case BEAMERCOMMAND_GETSTATE:
      beamer_state = BEAMERSTATE_UNKNOWN;
      Serial1.write("?1 Z03\r");
      break;
    case BEAMERCOMMAND_ON:
      Serial1.write("!1 U0F 1\r");
      break;
    case BEAMERCOMMAND_OFF:
      Serial1.write("!1 U0F 0\r");
      break;
  }

  reply_timeout = timestamp + REPLY_TIMEOUT;
}
