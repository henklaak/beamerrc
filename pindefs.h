#ifndef PINDEFS_H
#define PINDEFS_H

// IO pin definitions
#define IN_OFF    (2)
#define IN_ON     (3)
#define OUT_RED   (5)
#define OUT_GREEN (6)
#define OUT_DIAG  (13)

#endif
