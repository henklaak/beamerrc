#include "leds.h"

// Led brightness definitions
#define LED_RED_BRIGHT (255)
#define LED_RED_DIMMED (0)
#define LED_RED_OFF    (0)

#define LED_GREEN_BRIGHT (255)
#define LED_GREEN_DIMMED (0)
#define LED_GREEN_OFF    (0)

#define BLINK_PERIOD  (500) // ms
#define BLINK_FAST_PERIOD  (200) // ms

bool blink_state = false;
unsigned long blink_timeout = 0;

bool blink_fast_state = false;
unsigned long blink_fast_timeout = 0;

LedState ledstate_red = LEDSTATE_OFF;
LedState ledstate_green = LEDSTATE_OFF;

LedState ledstate_red_prev = LEDSTATE_OFF;
LedState ledstate_green_prev = LEDSTATE_OFF;


/**************************************************************************************************/
void leds_init()
{
  blink_state = false;
  blink_timeout = 0;
  blink_fast_state = false;
  blink_fast_timeout = 0;
  ledstate_red = LEDSTATE_OFF;
  ledstate_green = LEDSTATE_OFF;
  ledstate_red_prev = LEDSTATE_OFF;
  ledstate_green_prev = LEDSTATE_OFF;
  
  pinMode(OUT_RED, OUTPUT);
  pinMode(OUT_GREEN, OUTPUT);

  analogWrite(OUT_RED, 0);
  analogWrite(OUT_GREEN, 0);
}

/***********************************************************************************************/
void leds(unsigned long timestamp)
{
  if (timestamp >= blink_timeout)
  {
    blink_state = !blink_state;
    blink_timeout = timestamp + BLINK_PERIOD;
  }

  if (timestamp >= blink_fast_timeout)
  {
    blink_fast_state = !blink_fast_state;
    blink_fast_timeout = timestamp + BLINK_FAST_PERIOD;
  }

  int led_red_pwm = 0;
  if (ledstate_red != ledstate_red_prev || ledstate_green != ledstate_green_prev)
  {
    // For change detection
    ledstate_red_prev = ledstate_red;
    ledstate_green_prev = ledstate_green;

    // Start blinking pattern from scratch
    blink_state = 0;
    blink_timeout = timestamp + BLINK_PERIOD;
  }
  switch (ledstate_red)
  {
    case LEDSTATE_OFF:
      led_red_pwm = LED_RED_OFF;
      break;
    case LEDSTATE_DIMMED:
      led_red_pwm = LED_RED_DIMMED;
      break;
    case LEDSTATE_BRIGHT:
      led_red_pwm = LED_RED_BRIGHT;
      break;
    case LEDSTATE_BLINK:
      led_red_pwm = blink_state ? LED_RED_BRIGHT : LED_RED_DIMMED;
      break;
    case LEDSTATE_BLINK_FAST:
      led_red_pwm = blink_fast_state ? LED_RED_OFF : LED_RED_DIMMED;
  }
  analogWrite(OUT_RED, led_red_pwm);

  int led_green_pwm = 0;
  switch (ledstate_green)
  {
    case LEDSTATE_OFF:
      led_green_pwm = LED_GREEN_OFF;
      break;
    case LEDSTATE_DIMMED:
      led_green_pwm = LED_GREEN_DIMMED;
      break;
    case LEDSTATE_BRIGHT:
      led_green_pwm = LED_GREEN_BRIGHT;
      break;
    case LEDSTATE_BLINK:
      led_green_pwm = blink_state ? LED_GREEN_BRIGHT : LED_GREEN_DIMMED;
      break;
    case LEDSTATE_BLINK_FAST:
      led_green_pwm = blink_fast_state ? LED_GREEN_OFF : LED_GREEN_DIMMED;
      break;
  }
  analogWrite(OUT_GREEN, led_green_pwm);
}
