#ifndef BEAMER_H
#define BEAMER_H

typedef enum
{
  BEAMERSTATE_UNKNOWN=-1,
  BEAMERSTATE_OFF=0,
  BEAMERSTATE_ON,
  BEAMERSTATE_COOL,
  BEAMERSTATE_EMERGENCY
} BeamerState;

typedef enum
{
  BEAMERCOMMAND_GETSTATE,
  BEAMERCOMMAND_ON,
  BEAMERCOMMAND_OFF,
} BeamerCommand;

extern BeamerState beamer_state;
extern String replyString;
extern unsigned long reply_timeout;

void beamer_init();
bool beamer_is_busy(unsigned long timestamp);
void beamer(unsigned long timems, BeamerCommand cmd);

#endif
