
#include "beamerrc.h"
#include "pindefs.h"
#include "leds.h"
#include "buttons.h"
#include "diag.h"
#include "beamer.h"

#define POLL_TIMEOUT  (5000) // ms

State state = STATE_UNINITIALIZED;
unsigned long poll_timeout = 0;
bool automatic_on=false;

/***********************************************************************************************/
void (*resetFunc) () = 0;

/***********************************************************************************************/

void setup() {
  state = STATE_UNINITIALIZED;
  poll_timeout = 0;
  automatic_on = false;

  diag_init();
  leds_init();
  buttons_init();
  beamer_init();
}

#if 0
unsigned long lasttime = 0;

void loop()
{
#define BTN_PRESSED (HIGH)
#define LED_RED_BRIGHT (255)
#define LED_RED_DIMMED (64)
#define LED_GREEN_BRIGHT (255)
#define LED_GREEN_DIMMED (64)

  unsigned long timestamp = millis();
  bool toggle = (timestamp % 1000) >= 500;
  bool poll   = (timestamp - lasttime > 5000);

  digitalWrite(OUT_DIAG, toggle);

  bool off_pressed = (digitalRead(IN_OFF) == BTN_PRESSED);
  analogWrite(OUT_RED, off_pressed ? LED_RED_BRIGHT : LED_RED_DIMMED);

  bool on_pressed = (digitalRead(IN_ON) == BTN_PRESSED);
  analogWrite(OUT_GREEN, on_pressed ? LED_GREEN_BRIGHT : LED_GREEN_DIMMED);

  if (poll)
  {
    lasttime = timestamp;
    Serial1.write("?1 Z03\r");
  }
}
#else
/***********************************************************************************************/
void loop()
{
  unsigned long timestamp = millis();

  switch (state)
  {
    case STATE_UNINITIALIZED:
    case STATE_GETTING_INITIAL_BEAMER_STATE:
      ledstate_red = LEDSTATE_BLINK_FAST;
      ledstate_green = LEDSTATE_BLINK_FAST;
      break;
    case STATE_OFF:
      ledstate_red = LEDSTATE_BRIGHT;
      ledstate_green = LEDSTATE_DIMMED;
      break;
    case STATE_WAIT_FOR_ON:
      ledstate_red = LEDSTATE_DIMMED;
      ledstate_green = LEDSTATE_BLINK;
      break;
    case STATE_ON:
      ledstate_red = LEDSTATE_DIMMED;
      ledstate_green = LEDSTATE_BRIGHT;
      break;
    case STATE_WAIT_FOR_OFF:
      ledstate_red = LEDSTATE_BLINK;
      ledstate_green = LEDSTATE_DIMMED;
      break;
  }

  leds(timestamp);
  diag(timestamp);
  buttons(timestamp);

  serialEvent1();

  if (beamer_is_busy(timestamp))
  {
    return;
  }

  switch (state)
  {
    case STATE_UNINITIALIZED:
      beamer(timestamp, BEAMERCOMMAND_GETSTATE);
      state = STATE_GETTING_INITIAL_BEAMER_STATE;
      break;

    case STATE_GETTING_INITIAL_BEAMER_STATE:
      switch (beamer_state)
      {
        case BEAMERSTATE_OFF:
          state  = STATE_OFF;
          automatic_on = true;
          break;
        case BEAMERSTATE_ON:
          state  = STATE_ON;
          break;
        case BEAMERSTATE_COOL:
          state  = STATE_WAIT_FOR_OFF;
          poll_timeout = timestamp;
          break;
        default:
          state = STATE_UNINITIALIZED;
          break;
      }
      break;

    case STATE_OFF:
      if (btnOnPressed || automatic_on)
      {
        beamer(timestamp, BEAMERCOMMAND_ON);
        poll_timeout = timestamp;

        automatic_on = false;
        state = STATE_WAIT_FOR_ON;
      }
      break;

    case STATE_WAIT_FOR_ON:
      if (beamer_state == BEAMERSTATE_ON)
      {
        state  = STATE_ON;
      }
      else if (timestamp >= poll_timeout)
      {
        beamer(timestamp, BEAMERCOMMAND_GETSTATE);
        poll_timeout = timestamp + POLL_TIMEOUT;
      }
      break;

    case STATE_ON:
      if (btnOffPressed)
      {
        beamer(timestamp, BEAMERCOMMAND_OFF);
        poll_timeout = timestamp;

        state = STATE_WAIT_FOR_OFF;
      }
      break;

    case STATE_WAIT_FOR_OFF:
      if (beamer_state == BEAMERSTATE_OFF)
      {
        state  = STATE_OFF;
      }
      else if (timestamp >= poll_timeout)
      {
        beamer(timestamp, BEAMERCOMMAND_GETSTATE);
        poll_timeout = timestamp + POLL_TIMEOUT;
      }
      break;

    default:
      resetFunc();
      break;
  }
}
#endif // #ifdef HWTEST


void serialEvent1()
{
  while (Serial1.available())
  {
    char ch = (char)Serial1.read();

    if (ch != '\r')
    {
      replyString += ch;
    }
    else
    {
      if (replyString.startsWith("@1 0"))
      {
        int length = replyString.length();
        switch (length)
        {
          case 4: // Pattern "@1 0"
            break;
          case 9: // Pattern "@1 0 0000"
            beamer_state = (BeamerState)(replyString.substring(5).toInt());
            break;
          default:
            break;
        }
      }
      else
      {
        // some eror, just ignore it for the time being
      }

      reply_timeout = 0;

      // clear the reply buffer
      replyString = "";
    }
  }
}
