#include "diag.h"

#define DIAG_LENGTH    (16)
#define DIAG_PERIOD    (250) // ms

bool diagcode[DIAG_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0,
                              0, 0, 0, 0, 0, 0, 0, 0};

int diag_state = 0;
unsigned long diag_timeout = 0;

/**************************************************************************************************/
void diag_init()
{
  diag_state = 0;
  diag_timeout = 0;
  
  pinMode(OUT_DIAG, OUTPUT);
  digitalWrite(OUT_DIAG, LOW);
}

/**************************************************************************************************/
void diag(unsigned long timestamp)
{
  if (timestamp >= diag_timeout)
  {
    diag_state++;
    if (diag_state >= DIAG_LENGTH)
    {
      diag_state = 0;
    }
    diag_timeout = timestamp + DIAG_PERIOD;
  }

  for (int i = 0; i < DIAG_LENGTH; ++i)
  {
    diagcode[i] = 0;
  }

  switch (state)
  {
    case STATE_UNINITIALIZED:
      diagcode[ 0] = 1;
      break;
    case STATE_GETTING_INITIAL_BEAMER_STATE:
      diagcode[ 0] = 1;
      diagcode[ 2] = 1;
      break;
    case STATE_OFF:
      diagcode[ 0] = 1;
      diagcode[ 2] = 1;
      diagcode[ 4] = 1;
      break;
    case STATE_WAIT_FOR_ON:
      diagcode[ 0] = 1;
      diagcode[ 2] = 1;
      diagcode[ 4] = 1;
      diagcode[ 6] = 1;
      break;
    case STATE_ON:
      diagcode[ 0] = 1;
      diagcode[ 2] = 1;
      diagcode[ 4] = 1;
      diagcode[ 6] = 1;
      diagcode[ 8] = 1;
      break;
    case STATE_WAIT_FOR_OFF:
      diagcode[ 0] = 1;
      diagcode[ 2] = 1;
      diagcode[ 4] = 1;
      diagcode[ 6] = 1;
      diagcode[ 8] = 1;
      diagcode[10] = 1;
      break;
  }

  digitalWrite(OUT_DIAG, diagcode[diag_state]);
}
