#ifndef LEDS_H
#define LEDS_H

typedef enum
{
  LEDSTATE_OFF=0,
  LEDSTATE_DIMMED,
  LEDSTATE_BRIGHT,
  LEDSTATE_BLINK,
  LEDSTATE_BLINK_FAST,
} LedState;

extern LedState ledstate_red;
extern LedState ledstate_green;

void leds_init();
void leds(unsigned long timestamp);

#endif
